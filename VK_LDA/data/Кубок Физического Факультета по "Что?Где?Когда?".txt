Сегодня последний день регистрации    Лучшая команда физфака официально в финале межфакультетского кубка! Подробные результаты завершившегося турнира. 
В Кубке приняло участие 31 команда. Увы, но у нас не было возможности вместить всех желающих, которых набралось 43 команды. 
15 команд представляли хозяев поля, 4 команды разыгрывали звание лучшей команды ФИЯР, 12 команд принимало участие в общем зачёте. 
Первая тройка общего зачёта:
1. Пей чай
2. Ничего не придумали
3. Где венок из остролиста
Интересно, что каждой группе команд досталось ровно по одному призовому месту.

Первая тройка зачёта физического факультета:
1. Пей чай (теперь - участники финала межфакультетского кубка)
2. Армия МурАвьев
3. Почему такие трудные вопросы?
Вместе с ними на дуэль против химического факультета вызываются занявшие 4 место команда Великий Гюйгенс. 

В письменных конкурсах борьба вышла не менее ожесточённой. 
Первая тройка выглядит так:
1. Зато мы клёвые
2-3. Пушистики
2-3. Альтависта

Лучше всех из физиков с даугавпилс справился коллектив "Почему такие трудные вопросы".

Полные результаты всех команд доступны в прикреплённых файлах. 

Оргкомитет благодарен всем командам, пришедшим на игру. 
Мы выражаем сожаление о том, что не могли пустить всех желающих. 
Для физфаковских команд будет организован дополнительный закрытый турнир после праздников. На нём мы будем готовиться к межфакультетскому кубку, докомплектовывать составы, получать ответы на интересующие нас вопросы. И, конечно, играть! Турнир подошел к концу и мы рады представить Вам победителей среди команд физического факультета:

1. Пей чай
2. Армия МурАвьев
3. Почему такие трудные вопросы?

Поздравляем команды! 👏👏👏👏👏

А вот долгожданные ответы на письменные конкурсы! За 11 часов до конца приёма заявок желание сыграть турнир изъявило 42 команды!
Полный список заявок доступен по этой ссылке:
https://docs.google.com/spreadsheets/d/1ZlKkdIrcwXVxuyp-wcmYinQS9jhTIBkhkQqqt_GhTwM/edit?usp=sharing

Напоминаем командам о том, что вместимость нашей аудитории составляем 25 команд. Это значит, что, к сожалению, мы не сможем вместить всех желающих (но кое-что постараемся придумать). 
Уважаемые команды! Если по той или иной причине вы понимаете, что не сможете принять участие в турнире, ОЧЕНЬ ПРОСИМ ВАС, сообщите об этом. Каждое такое сообщение обрадует как минимум одну команду. Уже совсем скоро прозвучит долгожданный гонг и начнется турнир!

Остается всего 4 дня.

А мы тем временем напоминаем вам о регламенте турнира, с которым можно ознакомиться по ссылке: https://vk.com/topic-115574677_34669266.

Собирайте команды, регистрируйтесь (форма по ссылке ниже), играйте и получайте удовольствие!
Мы ждем вас 4 марта в 19:00 на физфаке в аудитории 5-19! Перед турниром вполне можно и потренироваться. Вы уже в предвкушении игры, не так ли? 

А у нас тем временем регистрация идет полным ходом! Торопитесь принять участие! 

Представляем вашему вниманию список уже зарегистрировавшихся команд.

Команды физического факультета:
1. Почему такие трудные вопросы?
2. Пятая пара в субботу 
3. Армия МурАвьев
4. Здесь всё куплено
5. Фиолетовая ракета
6. Великий Гюйгенс
7. Шавуха и пивас
8. Пей чай
9. Зато мы клевые

Команды других факультетов:
1. Лёд9
2. Аномия
3. Борис Савинков
4. Алло, это черепаха?
5. Тайная комната-218
4. Где венок из остролиста
5. Генератор случайных сов
6. Пушистики  ЧТО НАША ЖИЗНЬ? - ИГРА!

Знакома эта фраза? Ну, конечно, она звучит в заставке телеверсии игры "Что?Где?Когда?"!

Итак, [club1514373|Московский университетский брейн-клуб: ЧГК в МГУ] и Физический Факультет приглашают вас принять участие в Кубке Физического Факультета по "Что?Где?Когда?".

Победитель турнира (при участии 6 команд с факультета) выходит в финал Межфакультетского Кубка!

Место: ФизФак, аудитория 5-19.
Время: 4 марта, 19:00.

Чтобы принять участие, нужно:

1) Собрать команду из 6 человек.
2) Придумать название команды.
3) Выбрать капитана.
4) Зарегистрировать команду здесь -> https://docs.google.com/a/physics.msu.ru/forms/d/1pwPjLsXkp9vkbcLPIAAa2jBZHwmTRdkZNzzllow5amA/viewform?c=0&w=1.
5) Прийти 4 марта на ФизФак.
6) Готово, вы великолепны!

"Что? Где? Когда" - игра, в которую может играть любая компания из 6 человек.
"Что? Где? Когда" - игра, в которой можно проявить себя.
"Что? Где? Когда" - игра, которая не надоедает.  "Что? Где? Когда" будет на физическом факультете.
Кубок физического факультета по "Что? Где? Когда?"
Дата: 4 марта
Место: физфак, ауд. 5-19
Время начала: 19-00

Приглашаются все желающие.
Регистрация обязательна
Форма регистрации: https://docs.google.com/forms/d/1pwPjLsXkp9vkbcLPIAAa2jBZHwmTRdkZNzzllow5amA/edit?usp=drive_web
Победитель турнира (при участии 6 команд с факультета) выходит в финал межфакультетского кубка. 

"Что? Где? Когда" - игра, в которую может играть любая компания из 6 человек. 
"Что? Где? Когда" - игра, в которой можно проявить себя. 
"Что? Где? Когда" - игра, которая не надоедает. 