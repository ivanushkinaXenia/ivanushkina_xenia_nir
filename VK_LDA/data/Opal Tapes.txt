Lumisokea - Transmissions From Revarsavr (2016)
С этим вот они гастролировали по Азиатским странам  🇯🇵 🇰🇷
#Lumisokea #Opaltapes Скоро. // Michael DeMaio –  Half Cross //
Year: 2015
Style: Experimental, Techno
#opltps // Siobhan – Southgate //
Year: 2014
Style: Techno, Industrial, Experimental
#opltps // Masks – Food Plus Drug (II) //
Year: 2014 
Style: House, Techno 
#opltps // Ford Foster & William Watts – Ford Foster & William Watts //
Year: 2014
Style: House, Techno
#opltps // Worker Parasite – Proletariat //
Year: 2014
Style: Techno, House, Leftfield, Acid
#opltps // Cremation Lily - Radiance And Instability //
Year: 2015
Style: Ambient // Basic House & Metrist - The World Is Order //
Year: 2015
Style: Abstract, House, Techno // James Place - An Entire Matchbook A Night //
Year: 2014
Style: Abstract, Ambient, House // KETEV - KETEV //
Year: 2014
Style: Abstract, Ambient, Techno // Austin Cesear & Stefan Jos - Austin Cesear & Stefan Jos //
Year: 2014
Style: Abstract, Ambient, Experimental, House, Techno
#opltps // Lumisokea - Contrapasso //
Year: 2013
Style: Abstract, Experimental
#opltps // S Olbricht - Deutsch Amerikanische Tragodie //
Year: 2013
Style: Abstract, Techno
#opltps // Kaumwald – Hantasive //
Year: 2014
Style: Ambient, Noise
#opltps // COIN  – Inside Palace //
Year: 2013
Style: Abstract, Ambient, Minimal, Experimental
#opltps // Dreamweapon  – Living In Hell On Earth //
Year: 2013
Style: Abstract, Techno, Industrial
#opltps // Lumisokea - Apophenia //
Year: 2013
Style:  Abstract, Experimental 
#opltps // OOBE - SFTCR //
Year: 2013
Style:  Abstract, Techno, House 
#opltps // Manse - Lying In Wait //
Year: 2013
Style: Abstract, Techno
#opltps // Patricia - Body Issues //
Year: 2013
Style: Ambient, Techno, House, Acid 
#opltps // Karen Gwyer - Kiki The Wormhole //
Year: 2013
Style:  Experimental, Techno, Ambient 
#opltps // Bleaching Agent - Stride By Stride //
Year: 2013
Style: Abstract, Techno 
#opltps // PHORK - Privileged Life //
Year: 2013
Style: Abstract, Ambient, Techno, Experimental 
#opltps // Shapednoise - Until Human Voices Wake Us //
Year: 2013
Style: Experimental, Rhythmic Noise, Industrial
#opltps // Yves De Mey - Metrics //
Year: 2013
Style: Ambient, Experimental, IDM
#opltps // Rejections - Resin In The Filter //
Year: 2013
Style: Noise, Abstract 
#opltps // Nikolai - The Symbols And The Signs //
Year: 2013
Style:  Abstract, Techno, Acid 
#opltps // Ñaka Ñaka - Juan Pestañas //
Year: 2013
Style:  Abstract, Ambient, House, Techno
#opltps // HOLOVR - Lunar Lake //
Year: 2013
Style:  Abstract, House, Ambient, Experimental 
#opltps // Body Boys - Growth Window //
Year: 2013
Style: Abstract, Ambient
#opltps // Lumigraph - Nautically Inclined //
Year: 2013
Style: House, Abstract
Full Release: vk.cc/4jOMkj
#opltps // Uio Loi - Cane //
Year: 2013
Style:  Abstract, Ambient, House
#opltps // COIN  – Stilled //
Year: 2013
Style: Abstract, Minimal, Techno, Industrial, Ambient
#opltps // S Olbricht – Deutsch Amerikanische Tragödie //
Year: 2013
Style: Abstract, Ambient, House, Techno
#opltps // Basic House – Caim In Bird Form //
Year: 2013
Style: Experimental, Techno, Minimal, Abstract
#opltaps // VA – No Lotion //
Year: 2013
Style: Abstract, Techno, House, Experimental, Industrial, Minimal
Full Release: vk.cc/4iv1HO
#opltapes // Various – Cold Holiday //
Year: 2012
Style:  Abstract, Techno, House, Ambient, Experimental 
Full Release: vk.cc/4iIHiG
#opltps // Traag - Confused In Reality //
Year: 2012
Style: Abstract, Techno, Industrial, Experimental
Full Release: vk.cc/4iIvMQ
#opltps // Ex-Con - Manual De Forca I Tactica //
Year: 2012
Style: Abstract, Techno, Industrial 
#opltps // IVVVO - All Shades Of White //
Year: 2012
Style:  Abstract, House, Techno 
Full Release: vk.cc/4iuHj7
#opltps // Personable & Dwellings & Druss – Mirror & Gate Vol. III //
Year: 2013
Style: Abstract, Acid, Techno, Experimental
#opltps // Rolling Acres, Hobo Cubes – Mirror & Gate Vol. II //
Year: 2013
Style: Ambient, Techno, Abstract
Full Release: vk.cc/4i3KPJ
#opltps // Prostitutes , Basic House – Mirror & Gate Vol. I //
Year: 2013
Style: Experimental, Noise, Abstract, Techno
Full Release: vk.cc/4i0tXd
#opltps // DJ Ford Foster - Function Trax Vol. 2 //
Year: 2012
Style:  Ghetto, Techno, House 
#opltps // MCMXCI - Skogen, Flickan och Flaskan //
Year: 2012
Style: Abstract, Techno 
#opltps // Wanda Group - Piss Fell Out Like Sunlight //
Year: 2012
Style:  Abstract, Experimental
#opltps // 51717 – 0VUL //
Year: 2012
Style:  Abstract, Techno 
#opltps // Basic House - I'm Not A Heaven Man //
Year: 2012
Style:  House, Abstract, Techno, Ambient 
#opltps // BAT & OND TON - BAT & OND TON //
Year: 2012
Style:  Abstract, Techno, House, Dub, Experimental 
Full Release: http://vk.cc/4igFQy
#opltps // Personable – Alternate/Other //
Year: 2012
Style:  Abstract, Acid
#opltps // Tuff Sherm & PMM - The Pagan Cinema //
Year: 2012
Style:  Abstract, Techno, Electro
Full Release: vk.cc/4ig2eD
#opltps // Huerco S. – Untitled //
Year: 2012
Style: Abstract, Deep House
#opltps // 1991 - High-Tech High-Life //
Year: 2012
Style: New Age, House, Abstract, Ambient
Full Release: vk.cc/4hVV0d
#opltps  