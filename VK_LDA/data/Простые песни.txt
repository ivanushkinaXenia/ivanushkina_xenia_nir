Робкое мартовское солнце потихоньку плавит осунувшиеся сугробы.
Повсюду под ногами бегут весёлые весенние ручейки, а значит, самое время пускать кораблики! И ещё один фестиваль, на который мы хотим поехать; Готовим нашим слушателям приятный сюрприз :) Тем временем продолжаем подавать заявки!
Хотим участвовать в фестивале независимой музыки Индюшата 2019.
#яхочунаиндюшата, #индюшата2019 Пингвин Live!
И, кстати, подписывайтесь на наш канал на YouTube. Мы будем его потихоньку наполнять. ✨🌈Ура, ура, ура!! Сегодня День Рождения нашего бессменного и незаменимого [id3846734|Саши]. Благодаря ему и его песням появилась эта группа. Желаем ему неиссякаемого источника вдохновения и оставаться всегда таким же веселым и жизнелюбивым не смотря ни на что! С Днем Рождения!! Защитника день
Концертом отметим мощным! Сегодня у Простых песен праздник!
Мы отмечаем день рождения [id21442025|Вити!]
Витя – это человек, в котором живёт музыка! 
Он сочиняет музыку и песни, играет на всех видах гитар, на клавишах, барабанах и губной гармошке.
Но это ещё не всё! Витя замечательный человек: настоящий друг, любимец  детей, знаток русской истории и литературы, талантливый программист!
Витя будь здоров и расти большой!
Мы тебя очень любим! Завтра увидимся! Друзья, концерт уже в эту субботу! Решили поучаствовать в конкурсе от [club76477496|ВКонтакте с авторами] (главный приз — запись в одной из европейских студий), а заодно вспомнить выступление на прекрасном фестивале, посвященном 80-ти летию Электростали.
#vktalents_redbullkitchen Готовимся к яркому лету!
Подали заявку на фестиваль
[club92011882|Уральская ночь музыки]
#UralMusicNight2019_application #Простыепесни #ProstiePesni А вот и наше видеоприглашение! Друзья, [club176581253|концерт] уже не за горами!
Приходите сами и приглашайте друзей! Хорошие новости для любителей музыки!
16 февраля наша любимая студия [club1589046|Robot Rock] отмечает день рождения!
На [club176581253|праздник] приглашаются все желающие!
Мы, естественно, тоже будем петь простые песни в честь праздника! Ногинск
Ул. Гаврилы Клюева 2. 
Мы начинаем в 19:30
Вы ещё успеете :) Внимание Ногинск! 
Простые песни едут в гости!
Выступаем с акустикой!
Добро пожаловать :) Вечером первого дня зимы в зале Электростальского дома культуры было очень холодно. На улице тоже было холодно и темно. 
На сцену стали один за другим выходить музыкальные коллективы. И с каждым новым аккордом, с каждой новой песней в зале становилось чуть теплее. Музыка разжигала огонь в сердцах всех участников фестиваля, и вскоре стало жарко как в сталеплавильном цехе!
Спасибо музыкантам, зрителям и организаторам за этот согревающий душу [club173765928|праздник]! И до новых встреч! Читайте интервью с группой Простые песни. 
Обязательно приходите на фестиваль 1-го декабря!
Такие события - редкость в нашем городе. Хочется рассказать о том, как появляются наши песни, как мы придумываем слова, музыку и аранжировки. 
Самое главное происходит в студии, когда мы собираемся вместе. Пространство и время преображаются. Начинается взаимодействие стихий. Барабаны, бас, гитары, клавиши и голоса сливаются в салюте гармоний и ритмов. А потом мы начинаем придавать всему этому форму. Порой это получается легко и естественно, а иногда на это уходят недели. 
Недавно на нашей репетиции побывала [id13575419|Анна Ратникова]. [id13575419|Аня] – фотограф и она подарила нам живые, атмосферные фотографии. Мы рады показать их вам, и пользуемся случаем, чтобы ещё раз сказать Ане спасибо! Настоящий музыкальный праздник с участием Простых песен состоится в первый день зимы! Прекрасное место, замечательная атмосфера!
Настоящий праздник музыки!
Спасибо вам, дорогие слушатели.
Спасибо Unlock cafe. Мы на месте!
Группа Акваланг готовится к саунд чеку. Потом мы ))  Простые песни - это уже не коллектив друзей и единомышленников.
Это почти как семья.
Тем радостнее появление в группе нового участника!
Интеллигентный, скромный, обаятельный гитарист с хорошим чувством юмора добавит звучанию «ПП» плотности, мелодичности и драйва!
Добро пожаловать [id155844941|Коля]!  !!!ВНИМАНИЕ!!! [club172694594|Концерт 2 ноября] переносится в [club153730614|Unlock cafe], мы сыграем также совместно с нашими хорошими друзьями, замечательной [club1004549|группой Акваланг]. Приходите! 

🏠 м.Китай-город, Лубянский проезд 19/1 
⌨ www.unlockcafe.su
Начало: 20-00 
Вход: плати сколько хочешь! Друзья! Рады сообщить, что 2 ноября в [club82904672|Lowenbrau Haus], мы сыграем [club172694594|концерт] совместно с нашими хорошими друзьями, замечательной [club1004549|группой Акваланг]. Приходите!
Москва, Lowenbrau Haus, Новорязанская ул., 26, стр. 1 
Начало: 20-00
Вход: 400р. В выходные едем поздравлять друзей с праздником! У Прстых песен появился новый друг. Актёр, режиссёр, мыслитель Антон Шутов. Делюсь ссылкой на один из его проектов, в котором нам довелось принять участие. Едем в Казань! 🔥Простые Песни едут в Казань! 🔥
В эту субботу, 8 сентября, выступим на эко-фестивале [club170132488| Укроп 2018 Долой Борщевик] под Казанью! Проявили видео со дня города! Что нас вдохновляет? Почему у нас рождаются новые песни? Что заставляет нас подолгу репетировать их в студии? 
Я скажу. 
Такие дни как вчерашний праздник! 
Люди, которые сделали его для нас. Люди, которые пришли послушать музыку! 
Вы не поверите, какие прекрасные, добрые лица мы видели со сцены! 
Как здорово быть частью всего этого! Спасибо вам!
И да, кстати, музыкальный праздник не закончился! В субботу на площади перед «ДК Октябрь» выступят наши любимые [club88775620|RED DATA CAT],  [club90890772|АГНИ] и много других интересных музыкантов! Праздник в полном разгаре!
Мы выходим на сцену в 19:00 Друзья! 28 августа сыграем небольшой сэт на празднике, посвященном 80-летию нашего города Электросталь! Вся информация во [club169934983|встрече], наш выход в 19-00. Лето, Солнце, здоровый образ жизни и Простые песни! 
В субботу в Глуховском парке г. Ногинск. [club119714008|Фестиваль Здорового Образа Жизни | 30 июня 2018] Большое спасибо организаторам, участникам и гостям фестиваля! 
Другая улыбка – словно уютный дом, освещённый внутренним светом в сером спящем городе. Дом, двери которого открыты для всех в чьём сердце горит огонь творчества и любви. Друзья! Мы будем рады видеть вас на фестивале "Другая улыбка" в подмосковном Ногинске в ближайшую субботу! Продолжая тему выборов:
Голосуем за Простые песни!
https://vk.com/@taman_fest-stan-chastu-ubileinoi-tamani-blok-11 Друзья! В следующую субботу состоится очень интересная встреча! Приглашаем всех желающих! Друзья, приходите на день рождения нашей любимой студии!
Простые песни открывают праздник!
Подробная информация на странице мероприятия: https://vk.com/rrock2018 Друзья! 
С большой радостью делимся с Вами нашей новой песней! 
Верьте, Надейтесь и Любите вместе с [club62249849|Простыми Песнями]! Друзья! Мы хотим, чтобы частичка лета поселилась в душе каждого.
И пусть в дождливый осенний день или морозный зимний вечер 
Вас греют воспоминания о жарких летних деньках. 
Смотрите наше видео, пойте Простые песни и будьте счастливы! 
Музыка и слова – Простые песни 
Сценарий, постановка и монтаж клипа – [id72152145|Никель Кирилл] Хорошие новости! 
Простые песни теперь в ротации радио Антифон!
https://vk.com/antifon_radio И ещё! Спасибо Курск! Курск. Антифон. По горячим следам :) Друзья, если Вы ещё не были в Курске, следующие выходные - самый подходящий момент это дело исправить! 
2 сентября Простые песни выступят на фестивале [club118124081|Антифон]. Ещё там можно будет услышать наши любимые группы: [club1004549|Акваланг] и [club30289436|Новые дни] и вообще будет много всего интересного! И еще немного видео с концерта в Антресоли! Вот как замечательно прошел [club151629037|концерт] 12 ого августа в городе Электросталь в кафе Антресоль. Спасибо всем кто пришел. а также [id1967466|Алексею Лебедеву] и всей команде [club1589046|студии звукозаписи Robot Rock Электросталь] за организацию и профессионализм. Простые песни и Red data cat 12 августа в Антресоли! 
Расскажите друзьям и приходите сами! "... мне, словно той реке, назад дороги нет." Еще одна новейшая песня от группы Простые Песни. Философские размышления на берегу реки. Друзья! Мы тут зашли на [club1589046|студию] к [id1967466|Алексею] и сыграли пару наших новых песен, а он снял их на видео и записал звук. Одну из песен рады Вам представить прямо сейчас! Хорошие новости! Простые песни готовят для Вас сюрприз! Ждать осталось недолго! Друзья, на улице снова теплеет, светит доброе майское солнышко, а значит пора садится на велосипед и ехать в парк! 
Нашу новую песню мы посвящаем открытию вело-сезона 2017!
Вперёд - на встречу солнцу и свободе!!! Друзья! В ближайшие выходные в Ногинске состоится открытый фестиваль искусств "Другая улыбка 2017"! В программе фестиваля поэзия, фотография, живопись, графика, мультипликация, театр и конечно Музыка!
Подробности на [club113453277|страничке мероприятия].
С радостью сообщаем, что "Простые песни" выступят на этом замечательном событии 26 марта в большом зале в 20:00 с акустической программой. Будем рады встрече с Вами! Сегодня мы с радостью поздравляем бессменного барабанщика "Простых песен" Евгения Крецу! Во многом благодаря Жену Простые песни нашли свой звук, его творческая энергия и впечатляющая музыкальная эрудиция вдохновляют нас и помогают двигаться дальше! 
Женя, с днем рождения! Простые Песни - участники премии "Jagermeister Indie Awards"! 
 
Друзья! Группа "Простые Песни" вошла в число претендентов на победу в номинации "Young Blood" премии "Jagermeister Indie Awards"! Первая цель - это попасть в итоговый топ-56, для достижения которой нам очень нужна ваша поддержка! 
Заходите на http://indieawards.jagermeister.ru/youngblood, найдите нас по названию и, предварительно авторизовавшись через соц. сеть, ставьте ЛАЙК рядом с нашей песней "Урюпинск"! 
Победителей ждут самые разнообразные призы - от съёмки видеоклипа до музыкального оборудования Друзья! У нас сразу две хороших новости: Санкт-Петербургское музыкальное издательство "Бомба-Питер" включило нашу песню "Урюпинск" в свой весенний сборник независимых исполнителей (https://oxota.bomba-piter.ru/releases/1095-ohota-64.html). 
А ещё мы приступили к съёмкам клипа на песню Лето и предлагаем Вам принять в этом непосредственное участие! Присылайте нам видеозаписи Вашего летнего досуга, которые соответствуют духу песни и могут быть включены в клип.
Каждому приславшему - майский сборник Бомбы на CD в подарок! 
Не забываем нажимать кнопку "поделиться"! Кроме нас на Джемфесте будет ещё много классной музыки! В ближайшие выходные "Простые песни" выступают на [club92739203|Джемфесте!] Ждём Вас на концерт! Фрагмент нашего выступления на благотворительном фестивале в Антресоли В эту субботу город Электросталь
Музыкальный благотворительный фестиваль
В календарях своих время отметим
Поддержим фонд [club122412324|Счастливые дети]
В 12:30 - Простые песни
Устроим прекрасный праздник вместе! Друзья! 
Простые песни участвуют в Народном Хит-параде! 
Наш трек попал в голосование Народного Хит-Парада. Голосование проходит по данной ссылке http://vk.com/topic-22228596_26961958. Голосуем!!! Нас поддерживают. Простые песни примут участие в Электростальском рок фестивале! Ногинское телевидение о фестивале "Другая улыбка" и группе "Простые песни"  Вчера был замечательный вечер! Всем огромное спасибо!  Друзья, в конце марта в городе Ногинск состоится фестиваль искусств. На фестивале будут представлены музыка, живопись, фотография, театр, поэзия и рукоделие. 
Этот праздник культуры будет длиться целых три дня 
с 25-ого по 27-ое марта! 
С радостью сообщаем, что Простые песни участвуют в этом удивительном событии. 
26 марта нам предоставляется целый час, в течение которого мы будем исполнять свои песни. 
Приходите, там будет здорово! Вход бесплатный!
Нажмите кнопку «поделиться», чтобы Ваши друзья тоже узнали об этом событии.
[club113453277|Нажмите сюда и окажетесь на странице мероприятия]  Хорошие новости, Друзья!
Мы с радостью сообщаем Вам, что теперь все наши песни, которые мы уже записали, можно послушать и скачать на сайте Soundcloud по вот этой ссылке: https://soundcloud.com/aleksandr-alkhimenko
Также послушать песни можно ещё на сайте Kroogi по ссылке https://prostye-pesni.kroogi.com/?locale=ru 

Туда же мы будем добавлять все новые песни по мере их записи! Дорогие друзья! Вечером 13 февраля Простые песни выступят на концерте, приуроченном ко дню рождения нашей любимой студии! Начало концерта в 19:00, адрес: Электросталь, ул. Тевосяна, 25, 5й этаж Whats App Pub подробности [club111561220|здесь] Хорошие новости! Простые песни решили отметить окончание этих замечательных зимних праздников концертом! Друзья, для многих сейчас непростое и даже тревожное время. Простые песни уверены, что всё будет хорошо. 
Главное помнить о вечных ценностях: о любви, дружбе, семье. Мы очень надеемся, что наша новая песня понравится Вам и сделает Вашу жизнь чуточку лучше!

Песня записана на студии [club1589046|Robot Rock] (г. Электросталь, ноябрь 2015 года)
Запись, сведение, гитара - [id1967466|Алексей Лебедев] 
Замечательные фотографии с видами Урюпинска предоставил Николай Гришин (http://kpok.livejournal.com/)

группа Простые Песни:
[id3846734|Александр Альхименко] - вокал, гитара, песни
[id6470637|Евгений Крецу] - ударные
[id21442025|Виктор Сихневич] - бас, микрокорг Сегодня в 22:00, ждём Вас в "Biker zone" , г. Ногинск, ул. Декабристов 38 Уже в эту пятницу! Не пропустите! Говорят, Он ещё и на машинке умеет строчить! Новая простая песня появится уже скоро! На днях записывали барабаны, а сегодня голос и гитары! Простые песни в [club91699275|Biker Zone!]
Первая пятница декабря.
Ждём всех на наш концерт! Дорогие друзья! Простые Песни рады представить свою новую осеннюю запись. Это таинственная душещипательная история Музыканта. Слушайте, наслаждайтесь и делитесь с друзьями.

Записано на студии [club1589046|Robot Rock] (г. Электросталь, август - сентябрь 2015 года)
Запись, сведение, соло-гитара - [id1967466|Алексей Лебедев] 
Иллюстрация - [id118027530|Елизавета Егорова]

группа Простые Песни:
[id3846734|Александр Альхименко] - голос, гитара, песни
[id6470637|Евгений Крецу] - ударные
[id21442025|Виктор Сихневич] - бас, микрокорг Наши хорошие друзья, группа Сахарный человек готовят к выпуску свой новый альбом и просят материально поучаствовать в его выпуске. Простые песни не раз выступали на одной сцене с Сахарным человеком, это замечательные весёлые люди, энтузиасты своего дела. Надеемся, что общими усилиями, необходимая сумма будет набрана и мы все сможем насладиться новой музыкой от СЧ! Простые песни вошли в топ 5 новинок "Нашего радио"! Ребята подскажите аккорды на песню самолет! Пожалуйста! Простые песни пока на втором месте! 
Голосование продолжится до 19-го августа.
http://battle.nashe.ru/ Сегодня, в пятом раунде "Битвы за эфир" на "Нашем радио" прозвучат "Простые песни", слушайте и голосуйте за нашу песню! Вчера мы побывали в творческой мастерской "Гуслица". 
Это совершенно удивительное место, пропитанное духом свободного творчества и самовыражения! Спасибо [id4382449|Анне] и другим организаторам нашего концерта, а также всем кто пришёл нас послушать! В ближайшее воскресенье 12 июля, "Простые песни" сыграют акустический концерт в арт центре "Гуслица" который находится в Орехово-зуевском районе, недалеко от Ильинского погоста. Приезжайте к нам на концерт, зовите друзей!
Информация о центре здесь: http://art-guslitsa.ru/
GPS-координаты для навигатора: 550 28 ’55 ″N 380 55′ 22″ E Друзья! 

Экспертный совет проекта "НАШЕ 2.0." выбрал "Простые песни" в качестве кандидата на участие в фестивале V-ROX 2015! 
Наши записи будут звучать на "Нашем радио" в "Битве за эфир" в четверг 16-го июля после 22:00. 
Если Вам нравятся наши песни, не упустите момент! Слушайте "Наше радио" и голосуйте за нас!

http://vrox.vladivostok3000.ru/news/1331/
http://www.nashe.ru/nashe20/ Наши друзья из [club41103657|чайной мастерской] в ближайшее воскресенье устраивают гаражную распродажу! Простые песни выступят там с акустической программой. Приходите, начало в 13:00! Вот [club95438439|страничка мероприятия]. Видеозаписи с сегодняшнего выступления! Рады сообщить, что "Простые песни" выступят на молодёжном концерте [club94344262|"Твоя жизнь"], который состоится в городском парке города Ногинск 31 мая в 17:00.
Скорее всего, мы сыграем там три-четыре песни. Что такое осень в детстве нам часто рассказывали в научно - популярных песнях, и теперь мы прекрасно это знаем.

 Но настало время узнать, что такое лето!

Ведь мало кто ждет осень или зиму летом, зато лето мы ждем всегда, тем более, что оно уже близко!

 Новая запись Простых Песен, студия [club1589046|Робот-Рок]. Вчера был замечательный концерт! Cпасибо пришедшим, за радушный приём и танцы, [id16240029|Сергею Гордиенко] за незримую поддержку йога-пространства, а [id2532547|Дэну Штольцу] за сказочный свет и убойный звук! Ждём Вас на [club94023332|концерт!] Друзья! 
Мы поздравляем Вас с прошедшими праздниками и с наступлением самого прекрасного месяца весны! 
Да здравствуют одуванчики и пение соловья по ночам!
Простые песни решили отметить все эти события концертом!
Приходите в ближайшее воскресенье (17 мая) в 17 часов в йога зал на ул. Первомайской в городе Электросталь.
Мы будем рады увидеть Вас там!
Подробнее о мероприятии на [club94023332|страничке события.]  Записываем новую песню в [club1589046|студии RR]. Хорошие новости! С помощью [club1589046|наших друзей] из студии "Робот рок" мы сделали студийное видео на песню Кораблик! 
Приятного просмотра! 