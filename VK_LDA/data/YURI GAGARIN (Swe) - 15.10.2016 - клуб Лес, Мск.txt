Билеты поступили в точки продаж. 

Подробнее тут:
https://vk.com/topic-124501493_34400530    Rope Sect — Personae Ingratae (2017) 
 
И без того прекрасный и надолго запоминающийся дебютный релиз Rope Sect в своей виниловой версии, вышедшей на Iron Bonehead Productions, был дополнен двумя бонусными треками, продолжающими через незаурядное хитросплетение дэт-рока, готик-рока и постпанковых мотивов рассказывать историю одного анонимного культа, затаившегося в густых лесах в ожидании скончания времён. 
 
[club163859229|Концерт уже 26 мая!] 
 
Билеты — vk.cc/7RYJue Abysmal Grief — Misfortune (2009) 
 
Дабы не томить фанатов столь долгим ожиданием, спустя два года после выхода дебютного альбома Abysmal Grief выпустили новую полноформатную работу с лаконичным названием «Несчастье». Не изменяя устоявшимся традициям, музыканты продолжили воспевать громогласные и полные патетики гимны печали и упадку, позволяя слушателю погружаться всё глубже в создаваемую ими уникальную атмосферу 
 
[club155476357|11.05.2018 - MOD club, Питер (Abysmal Grief, Infinity, Antiversum]) 
[club155478676|12.05.2018 - Rock House club, Москва (Abysmal Grief, Antiversum])      The Re-Stoned "Feedback". Спасибо ritual booking за организацию отличного концерта. Атмосфера была то, что надо 
👍 ...Yuri Gagarin оказались (оказался?) одной из тех групп, выступление которых невозможно описать, не срываясь во что-то, напоминающее скорее трип-репорт или нечто религиозное. В общем-то нам не привыкать, да и ясно было, что первый в России за черт знает сколько лет - если не первый вообще - спейс-роковый концерт будет именно таким. Мы хотели полетов во сне и наяву - мы их получили. Я лишь недавно начал понимать, что я вообще в музыке как таковой люблю в первую очередь именно абстрактное, слабо описуемое ощущение полета через плотный поток света - возможно, это что-то связанное со структурой мозга и человеческими предками или что-то эзотерическое, помнится, и Снежок так описывал свои детские видения рая - и исполнение музыки, вызывающей эти переживания, живьем способно размазать меня просто в кашу. В этот раз даже особо напрягать воображение не потребовалось - Свет и Туман сделали все сами. Почти все время сета музыканты людей напоминали слабо, а во многие моменты были вообще абсолютно не видны - да и не надо, и замечательно, ибо то, что лавиной неслось со сцены, не походило уже ни на старый добрый рокнролл, даром что приехало к нам прямиком из его золотых лет, ни вообще на что-либо рукотворное. Одно видение сменяется другим, еще более странным, хочется кричать во все горло или рухнуть на пол, но надо хотя бы попытаться сохранить рассудок; время от времени вспыхивает слэм - маленький, но отчаянный, массовые полеты со сцены, даром что она высотой по колено; с какого-то момента происходящее сложно назвать иначе как обратной стороной Луны - ужасно не хотелось писать эту банальность, но это так. Кто-то протягивает мне бутылку, а я уже не могу разобрать, вино в ней или пиво. Как когда-то на Faun, всерьез боюсь отрубиться, очень хочется сбежать и прервать пытку впечатлениями, но я знаю, что должен достоять до конца, чтобы не жалеть потом всю жизнь...
ЙОГ-СОТОТ! ЙОГ-СОТОТ! ЙОГ-СОТОТ!
Басист - его звали Лейф, о боги, ЛЕЙФ! Как же это клюквенно и мило! -  прыгает в зал, становится на ноги и раздает окружающим насвай. А потом возвращается на сцену и продолжает Жарить. В голове уже пусто, происходящее слишком безумно чтобы вообще с чем-то его ассоциировать. Перед сценой плещется флаг Швеции, и теперь всегда все будет хорошо. 
Разумеется, я был в полностью пережеванном состоянии, из которого окончательно не вышел до сих пор и которое надеюсь когда-нибудь поймать уже навсегда. Бродил по клубу с мыльными пузырями, обнимался с музыкантами и, возможно, делал что-то еще более богоугодное, о чем просто не помню. Кому-то досталось и сильнее - один персонаж вообще прямым текстом предлагал Лейфу люси-с-алмазами.
Я не знаю, кем надо быть, чтобы за час с небольшим открыть в маленьком московском подвале сквозную дыру через весь Космос и заставить толпу хипстеров, блэкеров и каких-то странных людей с концертов Сруба плясать как один под его ритмы. Я знаю, что такие концерты и такие люди жизненно необходимы нашему миру, без них он был бы хоть ненамного, да скучней и печальней. Дальше - больше: через пару недель нас научит любви ко всему живому психоделический десант Храма Психической Юности, а ровно неделей позже великий Акира Ямаока обрушит в океан такого трансцедентного добра, против которого бессильны будут все ужасы богатой японской фантазии. Впереди только космос. Keep mouving, don't look back! Девушка с камерой, стоявшая около правого портала, найдись. Второй ракурс мне и [id4585491|Илья Липкин] пригодился бы весьма! Наши шведские друзья, Yuri Gagarin, отправились домой, забрав с собой бесценный багаж позитивных эмоций и впечатлений от столь тёплого и дружественного приёма в Москве и Петербурге. Они улетели, - но обещали непременно вернуться; [club129341323|эстафету же думных таинств музыканты передали своим канадским коллегам Zaum], с которыми нам совсем скоро предстоит отправиться в новое путешествие – на этот раз в сопровождении таинственного каравана через знойную пустыню, скрывающую в своих бескрайних песках множество видений и загадок. Немного снимков с выступления Y.G.
Остальные на следующей неделе на сайте www.mustdie.ru или тут: http://vk.com/mmustdie К удивлению - отличный звук в клубе. Круто было, кайфово. Кроме бара и платного выхода на улицу. А так безусловно ритуал букинг круто все делает, добротно! Есть видео/аудио сета The RE-STONED и сета YURI GAGARIN. 
Обещаем постепенно все сделать и опубликовать ☝✌😎 Где фотоотчёт можно посмотреть? Горжусь тем ,что в Москве есть банда под названием The Re-Stoned!p.s. Шведы вселили в меня дух Гагарина ! А я ведь почти из того времени !))) Один из лучших ивентов в этом году, всё полностью круто! Спасибо всем участникам! Это то самое чувство, которое я хотел вновь испытать после посещения концерта Stoned Jesus года два назад. Чувство, что можно писать трип-репорт тогда, когда в твоей крови нет ни капли наркотических веществ, а ебануло так, будто ты выдул целый корабль нахуй. Усиленное примерно в три раза. Так вот о чём я хотел сказать... Пожалуй о том, что ради такой музыки, наверное, и придумали живые выступления. Когда не ощущаешь вокруг никого, находясь в полном единении с живым музыкальным потоком, плавая где-то в невесомости своего макрокосмоса. Да, огромной такой холодной бездны с мириадами звезд и пестрящими вдалеке туманностями и галактиками. Но ещё большее мастерство нужно иметь, чтобы вызвать подобное состояние. За это в том числе
спасибо отличному разогреву, задавшему нужный вектор. В первую очередь самим шведам, живое выступление которых останется непревзойденным, пожалуй, на очень долгий срок. И клубу "Лес", который я посетил впервые, и который, к моему восторгу, имеет просто потрясный звук и акустику помещения. Ну и оргам, что имеют смелость возить поистине охуенные, имеющие самобытность и настоящее музыкальное наследие и ценность коллективы. Такое надо продвигать и поддерживать всеми силами, что и будем стараться делать. Спа-си-бо! Лав ❤ Очень круто. Спасибо! Ах, какая же совершенно космическая красота была!
Спасибо)
❤️ Спасибо Олегу за хороший концерт, все прошло отлично! 
🎸✌👌 Респект вам черти от угоревшего космонавта !))) Так и знал, что этот трэк в качестве эпилога будет. Вот только мощнейший проигрыш в конце они как-то смазали, а я его так ждал. А в целом было очень круто. Всем спасибо за атмосферу. Замечательно!!!!! Обе команды достойней некуда!!! Спасибо организаторам за великолепный вечер!!! Полёт нормальный!

Расписание на сегодня:

19:30 двери 
20:20 THE RE-STONED
21:40 YURI GAGARIN

Цена на входе 1300  Просим любить и жаловать! 
 
Специальный гость вечера - [club6697869|THE RE-STONED] ! Yuri Gagarin – At The Center Of All Infinity (2015) 
 
Второй полноформатный альбом и последний на данный момент релиз коллектива своим звучанием неуклонно продолжил гнуть линию глубоких космических исследований через загадочную психоделическую музыку. Новеллой в этой части межгалактической саги Yuri Gagarin стало активное использование синтезаторов и шумов, то и дело посылающих зашифрованные послания от неземных форм жизни, с которыми человечеству еще только предстоит встретиться в далёком будущем. Хотя, возможно это «далёкое» намного ближе, чем мы думаем? 
 
Не упустите шанс разгадать загадки бескрайней Вселенной [club124501493|15 октября в Москве]

билеты - vk.cc/5x7wkP Yuri Gagarin – Sea of Dust (2015) 
 
Второй релиз шведов – мини-альбом из двух треков, вобравший в себя еще больше психоделичности и динамичности звучания, выдавая прекрасный саундтрек для путешествий в открытом космосе, где спокойный полёт сквозь Млечный Путь резко сменяется метеоритным дождём, сопровождающимся перегруженными риффами и ритмичными соло, не дающими спускать глаз с путеводной звезды, указывающей путь, а далеко на горизонте маячат неизведанные планеты и новые открытия. 
 
Отправьтесь исследовать таинственные уголки космоса вместе с нами [club124501493|15 октября в Москве]

билеты - vk.cc/5x7wkP vk.com/wall-52492448_1835

Follow vk.com/localmsk !  Фирменные билеты по 1100 руб всегда в продаже в рок галерее Зиг-Заг! Только что пришли отличные новости! Музыканты получили визы!  Yuri Gagarin – Yuri Gagarin (2013) 
 
«Самочувствие хорошее. Настроение бодрое, продолжаю полёт..» - с таких близких и знакомых русскоязычной публике слов великого космонавта начинается история группы Yuri Gagarin и их одноимённого дебютного альбома. Проходит буквально мгновение, как вдруг земля резко уходит из под ног, а тело ближайшие на 36 земных минут отправляется дрейфовать в невесомости по просторам бескрайнего психоделического космоса, освещаемого мириадами далёких звёзд и неизведанных планет. 

Билеты - http://vk.cc/5x7wkP Билеты появятся в продаже уже в эту пятницу! Следите за новостями! Рассказ "Времени нет" на музыку Yuri Gagarin.

Стоишь у стола, "крутишь" много фарша на старинной мясорубке, закатываешь концэрвы, из кастрюль валит пар. Стол на камбузе. Обшивка камбуза дребезжит от выхлопов новых динамиков. Камбуз в космическом корабле. Корабль на автопилоте, уворачиваясь от космического мусора, проходит мимо Плутона. Прочь из Солнечной системы. За старшим Белым братом. Наугад.   