Россия — лидер в освоении космоса. И сегодня мы расскажем о наших космических достижениях. «Писать грамотно — модно».
Сегодня проходит ежегодная образовательная акция «Тотальный диктант». Если вы хотите проверить, насколько хорошо знаете родной язык, тогда вам сюда.
Для этого всего лишь нужно заполнить анкету на официальном сайте и хорошее настроение.

А вы знали, что его пишут по всему миру? Кафе «Юность» — легендарное заведение Мурманска, работающее уже несколько десятков лет. После восстановления оно преобразилось и продолжает радовать своих посетителей.

Мурманск, Россия. Сулакский каньон

Автор — Марат Магов После реставрации здание МХТ им. А. П. Чехова по-настоящему расцвело.

Москва, Россия. Российский грузовой корабль «Прогресс МС-11» стал настоящим рекордсменом в истории МКС. Совершив 2 витка вокруг Земли, он долетел до станции за 3 часа и 22 минуты. Лисица на фоне вулкана Вилючинский. Камчатка Церковь Воскресения Христова — крупнейший и не имеющий аналогов храмовый комплекс  на территории Ханты-Мансийского края.

Ханты-Мансийск, Россия. Поздравляем вас с Днём космонавтики!

#ЦифраДня #Денькосмонавтики #Топ5 #Москва Восстановленный собор Непорочного Зачатия Пресвятой Девы Марии – самый крупный готический собор в России.

Москва, Россия. 12 апреля в России отмечают День космонавтики. 

Не бойтесь мечтать, ведь мечты добавляют красок в нашу жизнь и ведут нас к целям. Териберка. Кольский полуостров У Мурманска есть своя визитная карточка – центральный стадион.
За 59 лет существования он вырос до европейского уровня.

Мурманск, Россия. Слово «шляпа» имеет немецкие корни и пришло в русский язык в конце XVI – начале XVII в.

В немецком языке глагол slappen означает – «виснуть, свисать, висеть», поэтому головной убор с мягкими отвисшими полями стали называть Slappe.
В России название приобрело более широкое значение. Озеро Зеркальное. Кавказ По мнению министра культуры России Александра Соколова, в 2006 году в Ханты-Мансийске состоялось эпохальное событие — был открыт памятник равноапостольным Кириллу и Мефодию.

Ханты-Мансийск, Россия. Запланировано более 40 гастрольных выездов московских театров по России и за рубеж. Кроме того, в столичных театрах запланированы 360 премьер.

#ЦифраДня #Москва В футболе Марио Фернандес с ранних лет и когда-то играл за сборную родной Бразилии.

После чемпионата в 2009 году молодым игроком заинтересовались европейские клубы, однако контракт он подписал с московским ЦСКА и с 2012 года является его защитником.
По окончании пяти лет футболист не захотел покидать российскую сборную и продлил контракт.

По словам Марио, переехав в Россию он обрёл Бога и поменял свою жизнь. Заброшенные здания не только портят внешний облик города, но и создают угрозу для жизни и здоровья граждан.

Челябинск, Россия. #ЦитатаДня #Соловьев Футбольное поле из пластиковых стаканчиков? Да-да, это не шутка.
Оно находится в Сочи, а для его создания использовали 50 000 стаканов, собранных после чемпионата мира по футболу. После реставрации улица преобразилась, но сохранила свой исторический облик.

Москва, Россия. Возраст — это всего лишь цифра и Екатерина Дзалаева яркое тому подтверждение. В свои 83 года она проходит по 48 километров горных дорог, чтобы раздать людям почту.
Профессию она выбрала еще в детстве, когда наблюдала, с какой тревогой и надеждой люди ждали письма с фронта.

Женщина не раз признавалась, что благодаря работе чувствует себя счастливой и остается в строю. Зуб Суфруджу. Домбай. Кавказ Население Сургута неуклонно растёт. Город разрастается не только вширь, но и ввысь.

Сургут, Россия. #ЦифраДня #ЭтоРоссияДетка Многие жители России мечтают съездить на Байкал. А вы видели самое глубокое в мире озеро? 😍 Сегодня мы расскажем о том, сколько раз и при каких правителях видоизменялся символ государства. Стройка закончится, а благоустроенные и комфортные улицы будут еще долго радовать жителей.

Санкт-Петербург, Россия. Симеиз. Крым За 7 лет существования программы расселения аварийного жилья в благоустроенные квартиры уже переехали 3662 бывших жителя «деревяшек».

Мурманск, Россия. Предупрежден – значит вооружен! Ученые СПбГУ создают прибор, определяющий рак легких по выдоху.

Эта разновидность рака одна из самых тяжелых. Из-за позднего диагностирования, на 3-4 стадиях, умирает более половины пациентов.

С новым подходом сама процедура диагностирования станет занимать не более 10 минут и определить рак легких можно будет на первой и второй стадиях без дорогостоящих анализов, что сохранит жизни многим людям. Каракал Семимильными шагами движется развитие южноуральской столицы, к 2020 году она принципиально поменяет свой облик.

Челябинск, Россия. Доля компании по итогам двух месяцев на российском рынке грузовых автомобилей полной массой свыше 14 т, согласно собственным оценкам «КамАЗа», выросла и достигла 50% против 41% за январь-февраль 2018 года.

#ЦифраДня #КамАЗ #Топ5 #Россия Обилие машин, разбитые дороги и неухоженные фасады — все это давно кануло в Лету.
В 2018 году завершился ремонт последнего интерьера в  жилом доме XVIII–XIX веков в Лебяжьем переулке.

Москва, Россия. Слоёный пирог по-мурмански. Кольский полуостров В Новосибирском зоопарке около 800 видов животных, многие из которых занесены в Красную книгу.

Сейчас в нем содержится более 11 000 особей, что делает его самым большим в России.

Хотели бы там побывать?

Новосибирск, Россия. Принято считать, что слово «хулиган» произошло от фамилии лондонского дебошира.

Патрик Хулиган доставлял столько хлопот, что его фамилия стала именем нарицательным, характеризующим человека, нарушающего общественный порядок. Сулакский каньон. Дагестан Соборная мечеть Санкт-Петербурга считается самой крупной в европейской части России.
В течение восстановительных работ отремонтировали минареты и кровлю, отреставрировали майолики на входных группах, а также молельный зал и купол над ним.

Санкт-Петербург, Россия. #ЦифраДня #Москва Экспозицию Ильи Репина в Государственной Третьяковской галерее можно будет посетить до 18 августа, потому что потом она станет передвижной.

160 живописных работ и около 100 произведений графики отправятся в путешествие по Волге на теплоходе «Илья Репин».

Выставка приурочена к 175-летию художника и продлится до конца октября 2019 года. Не торговый центр, а произведение современной архитектуры.

 «Гостиный двор» за 13 лет работы уже стал символом Ханты-Мансийска.

Ханты-Мансийск, Россия. #ЦитатаДня #Шнуров Горный Алтай Благодаря проекту «Безопасные и качественные автомобильные дороги» не только отремонтируют автомобильные трассы Хабаровска и Хабаровского района, но и реконструируют магистрали Комсомольска-на-Амуре.

Хабаровск, Россия. Фантастический фильм режиссера Джаника Файзиева, выходящий в прокат 17 октября 2019 года.

Над Россией навис огромный инопланетный корабль. На самом деле это стадион, на котором проходит космобол — игра, напоминающая футбол.
Когда она идет,  мир останавливается и все наблюдают за ней. Судьба планеты зависит от исхода каждого матча.

Делитесь в комментариях, появились ли у вас ассоциации с этим фильм? Подсолнечное солнце) Суммарное число автомобилей в этом году достигло 4,5 млн единиц, что на 5% выше прошлогоднего показателя.

По данным «Автостата», первое место по популярности в Москве занимает Lada.

Москва, Россия. Среди участников программы — руководители ведущих вузов Москвы и Санкт-Петербурга, в том числе МГИМО, ИТМО, МАИ, СПбПУ. Первый образовательный интенсив «Остров 10-21» прошел летом 2018 года на о. Русский. Здесь впервые отработаны принципиально новые подходы и практики интенсивного обучения с применением технологий искусственного интеллекта, сбором цифрового следа, формированием индивидуальных образовательных траекторий.

#ЦифраДня Подснежники на рассвете Скоро гоголевское выражение про две беды станет уже неактуально в Калининграде. Там в этом году на 8 адресах проведут капремонт  проездов и дорог, а на 10 обновят тротуары.

Калининград, Россия. Сегодня мы отмечаем День рождения Рунета. 
25 лет назад в этот день был зарегистрирован домен – «.Ru». Закат, село Дунилово «Нигде, ни в каком городе мира, звёзды не светят так ярко, как в городе детства...»

к/ф «Карнавальная ночь»

Москва, Россия. «Он всегда первым входил в горящее здание и последним покидал его»— вспоминают коллеги Евгения Николаевича Чернышева, начальника Службы пожаротушения Москвы.

На личном счету у полковника тушение 250 пожаров и множество спасённых людей. В Сомали даже была выпущена почтовая марка с его портретом. Евгений Николаевич потряс своим мужеством сомалийцев, спустившись почти с 500-метровой высоты.

Героически погиб в 2010 году при тушении бизнес-центра «Мирлэнд». Отдав приказ на отход сослуживцам, он двинулся осматривать помещения, в которых могли находиться люди. Через несколько минут обрушилась кровля.

Он спас всех людей, но свою жизнь уберечь не сумел. Курайская степь и Северо-Чуйский хребет. Алтай В 2019 году из бюджета региона на строительство социальных объектов будет потрачено более 1,3 млрд рублей, это в 1,6 раза больше чем в 2018 году.

Запланировано построить 3 детских сада, физкультурно-оздоровительный комплекс, спортивный центр с искусственным ледовым покрытием, а также здание областного Театра кукол.

Мурманск, Россия. Предприятие в Вологде, которое выпускает мороженое, уже давно сотрудничает с Израилем. Теперь его продукцию можно будет купить и в Африке.

#ЦифраДня #Мороженое Оптические иллюзии, комната нарушенной гравитации и зеркальный лабиринт – все это и не только ждет вас в самом большом музее чудес Краснодара.

В 6 залах «Джоуль парка» разместились более 70 экспонатов, благодаря которым гости узнают много нового о самых разных сферах науки. Сейчас с трудом можно представить, что в магазин можно ходить полдня и все это время стоять в очередях.

Санкт-Петербург, Россия. Зюраткуль. Южный Урал В советское время здание самого крупного пассажа Европы было отдано под Наркомпрод.

Сегодня ГУМ живёт так, как он был когда-то задуман — идеальный торговый город Москвы.

Москва, Россия. Учёные из Новосибирска создали «живую» вакцину от гриппа, которая более безопасна и хранится пять лет.

Основу обычных препаратов составляют ослабленные микроорганизмы, которые производятся на основе куриных и перепелиных эмбрионов и являются возбудителями болезней.
Новая вакцина производится на культуре клеток в лабораторных условиях, поэтому не способна вызывать заболевание. Тихое местечко на Колыме Храм Покрова Пресвятой Богородицы построили на фундаменте и по образцу старой церкви, разрушенной при советской власти.

Ханты-Мансийск, Россия. В обновленном вагоне предусмотрены диваны с подголовниками, индивидуальное освещение, электрические розетки и USB-порты. Обивочные ткани изготовлены из огнеустойчивых материалов. В коридоре есть фильтр для очистки воды с возможностью ее подогрева и охлаждения, а также сенсорная панель. Туалетная комната имеет душ, пеленальный столик, дозатор для мыла и бумажных полотенец, встроенный фен для рук. Кроме этого, основной идеей нового плацкартного вагона стала возможность создания личного пространства для пассажира с помощью шторок и ширм.

#ЦифраДня #РЖД #Топ5 #Forbes В 2006 году Сургут был признан лидером по количеству отремонтированных домов.

А в 2018 году стартовала новая программа, благодаря которой отремонтируют ещё 123 дома.

Сургут, Россия. Ординская пещера. Пермский край Благодаря ремонту муниципальных объектов в Челябинске создаётся комфортная городская среда.

Челябинск, Россия. Слово «индекс» пришло к нам из латинского языка в VII веке. Первоначально оно означало – «список книг, запрещённых церковью» и лишь в середине ХХ века приобрело привычное для нас значение почтового номера. Весна пришла в Якутию На месте старого ДК «Энергетик» построили здание филармонии, которое внесло свой вклад во внешний и внутренний имидж города.

Сургут, Россия. Проект будет полностью введен в эксплуатацию в 2019-2025 гг., что позволит трудоустроить не менее 200 человек.

Стоимость проекта 4,8 млрд рублей. На данный момент томаты выращивают на площади 10 га. А в дальнейшем агрокомбинат планирует расширить площади до 50 га. «В «Детройте» тренер мне не разрешает забивать сверху, говорит, что я могу получить травму. В УГМК мне таких запретов не ставили, так что ждите слэм-данка».

В 2008 году ряды баскетбольной команды из Екатеринбурга (УГМК) пополнила американка Деанна Николь Нолан. В том же году в интервью журналу "PROспорт", она заявила, что постарается стать третьим игроком в женском баскектболе, забившим сверху. К началу 2000-х Никольская улица была одной из самых ветхих в Москве.

В 2012 году началась ее реконструкция, и сегодня Никольская – это уютная и благоустроенная пешеходная зона.

Москва, Россия. #ЦитатаДня #Миронов Мыс Фиолент.
#Крым Новосибирск — ты просто космос!

Станция метро «Гагаринская», Новосибирск, Россия. Ване Шапранову из Владикавказа всего шесть лет, а он уже спас около сотни котов и собак.

Родители не ругают мальчика за принесённого в дом кота, а, наоборот, поддерживают его.
Вместе с папой Ваня подбирает с улиц бездомных зверушек и оказывает им помощь. Животным находят дом или передают на попечение приюта.

Малыш уверен, что спасение обездоленных – его призвание. А когда он вырастет, то будет спасать людей. Горный пейзаж.
#Дагестан Третьяковский проезд всегда был местом скопления роскошных бутиков. Чтобы лучше сохранить его исторический облик, дорогу в торговых рядах сделали пешеходной.

Москва, Россия. #ЦифраДня #Россия Россия омывается тремя океанами: Тихим, Атлантическим и Северным Ледовитым. А сколькими морями омывается Россия, включая одно внутреннее море? Что стало частью национальной ДНК и поражало захватчиков на Руси? 
Об этом и не только мы вам сегодня расскажем. Обновлённый сквер на Пяти Углах горожане оценили по достоинству. Отжившая свой век территория превратилась в современную зону отдыха.

Мурманск, Россия. В 90-е Петербург выглядел плачевно.

От нехватки средств люди устраивали блошиные рынки, на которых продавали свои вещи и технику, чтобы хоть как-то прожить.

Санкт-Петербург, Россия. Учёные из Владивостока разработали экзотическую смесь для приготовления напитка, который обладает антиоксидантными свойствами.

Главными ингредиентами такого коктейля стали цитрусы и тихоокеанские медузы ропилемы.

Разбавленный водой концентрат похож на фруктовый сок, но обладает повышенным содержанием аминосахаров, витаминов, флавоноидов и минеральных веществ. Рассвет в ущелье Адыл-Су При реконструкции аэропорта мозаичное панно «Мадонна с младенцем» решили сохранить.

Если внимательно приглядеться, то можно заметить, что Мадонна находится в самолёте и смотрит в иллюминатор.

Сургут, Россия. Список составлен журналом The Art Newspaper. Эрмитаж в 2018 году посетили 4,2 млн человек. Также в списке Музей Московского Кремля, Третьяковка, Государственный музей изобразительных искусств им. А.С.Пушкина и Музей современного искусства "Гараж".

#ЦифраДня #Эрмитах #Топ5 #ЭтоРоссияДетка Свой нынешний облик Боровицкая площадь обрела благодаря городской программе «Моя улица».

Москва, Россия. Шавлинское озеро. Горный Алтай Барнаул наконец-то вышел из экономического кризиса 90-х.
В городе обустраивают улицы, строят новые аллеи и кварталы.

Барнаул, Россия. Феодосия – город на юго-восточном побережье Чёрного моря.
Своё название он получил от греческого Θεοδόσιος (Феодосиос) – «Богом данная». 