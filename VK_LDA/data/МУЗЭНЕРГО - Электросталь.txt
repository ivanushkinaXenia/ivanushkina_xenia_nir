Максимальный репост!!!! а не съездить ли нам всем в Дубну на концерт? #москва

Будет состав, который не добрался до Электростали. Но это самое "наше", какое только может быть. Приходите в гости, агитируйте московских знакомых! Вновь мимо Электростали, но Москва и Егорьевск рядом ;) А вот не съездить ли вам, товарищи, в Сергиев Посад? До Дрезны, товарищи, совсем недалеко, между прочим.  До Электростали "МузЭнерго" что-то всё никак не может добраться вновь, однако Сергиев Посад всё-таки ближе, чем наша "базовая" Дубна. Добро пожаловать, если вы соскучились по нашим странным европейцам ;)  Rocqawali (Пакистан/Дания) Svin (Дания) Дания По ссылке ниже можно послушать Симона Вирша
https://soundcloud.com/simon-wyrsch Daniel Herskedal (туба, электроника) из Норвегии Концерт начался и на сцене первый коллектив из Голландии. Simon Wyrsch (кларнет)
https://www.youtube.com/watch?v=kxQadJ-S-eg Участник "МузЭнергоБэнда" Simon Wyrsch (кларнет)
Молодой швейцарский кларнетист Симон Вирш — из тех, кто предпочитает в самоописании ограничиваться сухими статистическими данными. Любопытный список коллективов и лидеров, с которыми ему доводилось играть и работать (среди них, например, Кёртис Фуллер, Рой Харгроув, Кармен Ланди, несколько больших оркестров и т.п.). Длинный список мест, в которых ему доводилось выступать. Впечатляющая фильмография из десятка фильмов, в основном немецкоязычных, к которым он приложил руку, будучи то исполнителем, то автором музыки. И дискография с его участием, в которой аж восемнадцать альбомов, в том числе выпущенных его собственным квартетом. Собственно, всё. В остальном Вирш совершенно прямолинеен: приходите на концерты, покупайте диски, нанимайте меня как студийного музыканта или преподавателя, вот телефон.
Подобная лапидарность для современных европейских музыкантов не редкость. Может сложиться обманчивое впечатление, что им нечего о себе сказать, однако они они просто предпочитают, чтобы цветистыми описаниями музыки занимались журналисты; сами же по себе они слишком заняты, успевая поработать на всей доступной музыкальной территории, и простому слушателю порой не так уж и очевидно, что именно такие востребованные «одиночки» определяют современное состояние джаза не меньше, чем иные зацикленные только на себе ансамбли со стабильным составом.
Собственно, именно этот подход Вирш и будет демонстрировать в «МузЭнергоТуре»: именно возможность три месяца вариться в непредсказуемом импровизационном котле, вступать во всё новые и новые ансамбли, предлагать свой голос новым партнёрам и лидерам — это и есть основная задача интереснейшего музыканта, способного играть как традиционный развлекательный джаз, так и академическую музыку.
http://www.simonwyrsch.ch/cms/website.php - сайт Симона Вирша Svin (Дания)
https://youtu.be/rL29j67xxO8 12ого июня МузЭнергоТур 2015 стартовал! http://vk.com/muzenergo - хроника гастрольных дней. Svin (Дания)

Датская музыкальная сцена, безусловно, тесно связана с основными демократическими принципами жизни этого небольшого государства как такового. Svin — хороший пример проникновения здоровой демократии и в музыку. Обращаясь порой к достаточно противоречивым темам (чего стоит название их второго альбома «Secretly We Are Gay»), не стесняясь использовать в своём музыкальном материале спорные образы и агрессивные звуковые решения, они, с одной стороны, отпугивают определённую часть аудитории внешними проявлениями своего искусства. Но с другой — дают сомневающимся возможность освободится от ненужных стереотипов, увидеть новое (и, без преувеличения, прекрасное) там, где на первый взгляд имеет место разве что очередной эксперимент. Какая разница, как называется альбом, если при всех его концептуальных посылах это чисто инструментальная музыка? Какая разница, что звучание группы может быть отнесено к року или даже панку, если её считают за честь поддерживать люди из национальной джазовой ассоциации? «Независимость, любовь и неограниченное разнообразие в искусстве» — вот своего рода лозунг Svin, и ему трудно возражать.

Помимо многочисленных туров по Европе, у Svin есть в активе совершенно уникальное достижение: в апреле 2015 года им предстоит выступление на фестивале 12 Points в Дублине, который традиционно демонстрирует 12 разностилевых коллективов из 12 стран Европы. Нетрудно представить, сколь тщателен отбор в эту программу. Svin (Дания)

Lars Bech Pilgaard (гитара)
Henrik Pultz Melbye (тенор-саксофон, кларнет)
Adi Zukanovic (клавишные)
Anders Vestergaard (ударные)

Квартет датских музыкальных радикалов образован в 2008 году, когда всем его основателям было около тридцати. Уже поварившись с котле современной импровизационной музыки, вобравшей в себя все мыслимые музыкальные влияния, музыканты сформировали вполне конкретные представления о своём творческом пути. Основа этих представлений — полная свобода в выборе как выразительных средств, так и стилистической базы для самовыражения. А вот главный ограничитель выглядит достаточно неожиданно для коллектива, который именуют то авангардным, то шумовым, то экспериментальным: обладая всей этой свободой, оставаться в осознаваемых рамках, видеть общую структуру создаваемой музыки, быть в состоянии обосновать любое своё спонтанное решение не случайными побуждениями, а чётким пониманием выбранных и использованных методов.

Характерно, что один из критиков, описывавших свои впечатления от дебютной работы квартета, вышедшей в 2010 году («Heimat»), употребил для её описания параллели не только с «энергичным и экстравертным африканским перкуссионным фестивалем» и не только с «подавленной агрессией в стиле Sonic Youth», но и с музыкой ни много ни мало Арво Пярта, поистине легендарного современного академического композитора. Подобный взгляд полностью соответствует и мнению самого коллектива, который прямо называет своей миссией создание по-настоящему серьёзного жанрового разнообразия «в мире, где всем заправляет кассовый аппарат музыкальной индустрии».
http://www.svin-music.com/ Rocqawali (Пакистан/Дания)
http://vk.com/video-92493518_170981564?list=3c09add0f5ea85c4c6  Rocqawali (Пакистан/Дания)

Ejaz Sher Ali (вокал, фисгармония)
Tin Soheili (гитара)
Jonas Stampe (гитара)
Tomas Nesborg (бас)
Stephan Garbowski (ударные) Первый альбом группы Rocqawali был записан классическим «старым» способом, за два дня, в общей студии и практически без наложений. Эйяз Шер Али поёт исключительно на урду и пенджаби, но смысл исполняемых им текстов интуитивно понятен. Слушатели соответствующего возраста услышали в этой музыке отголоски того, как творили Led Zeppelin и Grateful Dead; молодёжь же открыла для себя в этом звуке что-то новое (что, безусловно, есть хорошо забытое старое). Многочисленные восторженные рецензии, номинация на национальную музыкальную премию и прочие формальные достижения — лишние доказательства того, что этому коллективу удалось найти свою собственную, ещё никем не хоженную тропу. Rocqawali (Пакистан/Дания)

Ejaz Sher Ali (вокал, фисгармония)
Tin Soheili (гитара)
Jonas Stampe (гитара)
Tomas Nesborg (бас)
Stephan Garbowski (ударные)

В самом названии коллектива заложена полная расшифровка его стилистики: это поистине удивительное сочетание классического хард-рока европейской школы и духовной музыки каввали, рождённой на границе Пакистана и Индии. Показательно, что именно в Дании, стране образцово-показательной в смысле демократии и уважения к людям, родился этот стремительно набирающий популярность проект: материал Rocqawali демонстрирует не только естественность музыкального сотрудничества Востока и Запада, но и существование вполне конкретных религиозных, по глубинной своей сути, платформ, которым не противопоказаны ни светские ценности, ни прямое сосуществование с другими религиями. Музыка каввали, одним из наиболее известных современных голосов которой является непревзойдённый мастер Нусрат Фатех Али Хан, основана на достаточно специфической музыкальной традиции пограничных областей Индии и Пакистана и на суфийской поэзии — то есть искусстве безусловно мусульманском. Лирика каввали провозглашает любовь к богу, но не требует ни того, чтобы этот бог был определён во всех деталях, ни какого-то определённого ритуала в поклонении ему. Что в итоге? Духовная, в прямом смысле слова, музыка, глубина и неагрессивность которой так же привлекательны для носителей других культур и религиозных взглядов, как корневые негритянские спиричуэлс или древнее монастырское многоголосье Западной Европы.

Rocqawali, конечно же, идут дальше. Суфийский дух в этой музыке — определяющее, но не единственно важное. Вокалист Эйяз Шер Али представляет одну из наиболее древних пакистанских династий, которых многие жители страны прямо называют «хранителями огня», когда речь идёт о каввали. Но его соратники, граждане Дании, привносят в музыку группы не только рокерский дух; они обладают поразительно нестандартными судьбами (барабанщик Стефан Грабовски, например, родился в Кении, а гитарист Тин Сохеили — в Иране), имели отношение к нескольким ключевым инди-коллективам страны, зарекомендовали себя в создании музыки для фильмов, продюсировании других артистов (!) и даже в сопровождении международных театральных представлений. Неудивительно, что проект, который поначалу рассматривался как вполне прямолинейное «культурное сотрудничество» между Пакистаном и Данией, взрывным образом превратился в полноценную рок-группу со своим голосом, своей специфической энергией, своим собственным посланием миру. Как говорят сами музыканты, послание это — классически суфийское: «Любовь превыше всех религиозных, политических и физических границ».   Daniel Herskedal (туба, электроника) Daniel Herskedal (туба, электроника)
В 2010 году авторитетнейший портал All About Jazz выдвинул Даниэля Херскедала на звание «лучшего джазового тубиста года». Дискография Херскедала насчитывает уже десятки интереснейших записей — включая, например, программу «Spring Is Here» влиятельного британского композитора и бэндлидера Джанго Бейтса, славного среди прочего тем, что играть у него, не будучи настоящим виртуозом, попросту невозможно. В том же 2010-м его Magic Pocket получили награду «JazZtipendiat», которая позволила Даниэлю посвятить некоторое время композиции для большого оркестра. Сегодня он известен как композитор не только для джазовых составов, но и для хоров; среди его сочинений есть и предназначенные для использования в кино.

Выступления с сольной программой Херскедал начал сравнительно недавно (в 2012-м), но и на этом поприще добился заметных успехов. В его ближайших планах — выпуск сольного диска на британском лейбле Edition Records. В январе 2013-го, незадолго до инициированного «МузЭнерго» первого приезда в Россию, Даниэль принял участие в норвежском «фестивале ледяной музыки», проведённом при температуре -15 градусов, на котором все музыканты играли на сделанных изо льда инструментах. Теперь всему миру известно, что ледяная туба называется tubice… Еще один участник концерта - это Daniel Herskedal (Норвегия)

Даниэль Херскедал получил консерваторское образование в Трондхейме (Норвегия) и Копенгагене (Дания). Выбрав не самый обычный для импровизирующего музыканта инструмент, он, тем не менее, быстро оказался на виду у публики и критиков. Херскедал сегодня — исполнитель и композитор, заметный не только на национальной, но и на международной сцене. Одной из причин этого является тот факт, что он примерно поровну делит своё время между Норвегий и Данией; другой — то, что его туба крайне востребована в самых разных составах, от крупных традиционных би-бэндов (Trondheim Jazz Orchestra) до малых экспериментальных составов (здесь стоит выделить его дуэт с блестящим английским саксофонистом Мариусом Несетом). К числу его основных проектов, помимо упомянутых, относятся ещё и три ансамбля (City Stories, Magic Pocket, Listen) и трио с трубачом Бертом Локсом и пианистом Дирком Балтхаусом. Итого шесть составов — и это, по его собственным словам, только основные проекты, в которых он работает. Spinifex (Нидерланды)
http://vk.com/video-92493518_170980361?list=89b58dbf66abf2082a Spinifex (Нидерланды)
http://vk.com/video-92493518_170980356?list=f4195d7dd47db5f822 Spinifex (Нидерланды)  Spinifex (Нидерланды) Spinifex (Нидерланды)

Tobias Klein (альт-саксофон, кларнет), Германия
Jasper Stadhouders (гитара), Нидерланды
Gonçalo Almeida (бас), Португалия
Philipp Moser (ударные), Германия
Piotr Damasiewicz (труба), Польша (в туре 12-17 июня) Типичный концерт Spinifex — это равноправное пользование мощными грувами, постоянная импровизация, технически виртуозные соло, нервная ритмика и запредельная динамическая самоотдача. Неудивительно, что с момента возникновения в 2009 году группа объездила с концертами не только всю Европу, но и побывала, например, в Индии, а в декабре 2014-го впервые приезжала в Россию. В «МузЭнергоТуре» Spinifex будут участвовать как формация вполне традиционного толка (пусть и с регулярно меняющимся составом духовых инструментов), но в целом в их практике есть такие необычные под-проекты, как Spinifex Orchestra (с существенно расширенным составом духовых), Spinifex Tuba Band (с добавлением к составу сразу двух туб), Spinifex Bollycore (радикальная интерпретация индийской музыки для кино с участием нескольких танцоров) и так далее. Spinifex (Нидерланды)

Tobias Klein (альт-саксофон, кларнет), Германия
Jasper Stadhouders (гитара), Нидерланды
Gonçalo Almeida (бас), Португалия
Philipp Moser (ударные), Германия
Piotr Damasiewicz (труба), Польша (в туре 12-17 июня)
John Dikeman (саксофон), США (в туре 2-6 августа)
Oguz Büyükberber (бас-кларнет), Турция (в туре 13-22 августа)

Интернациональная сборная, базирующаяся в Амстердаме, - не просто музыкальный коллектив, но одновременно образ жизни. Группа работает в направлении, которое критики определяют как «смесь авангардного рока и свободного панк-джаза». Определение довольно расплывчатое, но, тем не менее, дающее некоторое представление о направлении творческих поисков; сами же музыканты видят это направление как «вызов в деле пересечения стилистических барьеров». Барьеры ими, действительно, пересекаются — вернее, даже попросту уничтожаются. В музыке Spinifex можно найти не просто становящееся общим местом внимание к музыкальному наследию стран, далёких от Европы; здесь поистине равноценно вообще всё, что существует в музыке, о которой им довелось узнать. Арабские и индийские мелодические ходы — пожалуйста. Дисциплинированность сложной композиторски выписанной музыки a la Европа середины XX века — без проблем. Анархизм современной свободной импровизации — будьте любезны. Строго структурированные схемы «математического» металла — есть и это. Словом, слушателю нужно быть готовым к самым радикальным неожиданностям. Мини-альбом Bangin' Bülows Nice Jazz Quartet можно послушать здесь: 
http://www.deezer.com/album/7795679 Bangin' Bülows Nice Jazz Quartet (Дания)
http://www.youtube.com/watch?v=P1ShvGTvPoY Bangin’ Bülows Nice Jazz Quartet (Дания)
Frederik Emil Bülow (ударные) Bangin' Bülows Nice Jazz Quartet
Mikas Bøgh Olesen (гитара) Bangin' Bülows Nice Jazz Quartet
Adrian Christensen (бас) Bangin’ Bülows Nice Jazz Quartet (Дания)
Jon Døssing Bendixen (клавишные) Bangin’ Bülows Nice Jazz Quartet (Дания)

Frederik Emil Bülow (ударные)
Jon Døssing Bendixen (клавишные) 
Mikas Bøgh Olesen (гитара)
Adrian Christensen (бас)

Датский квартет — из числа тех, о которых сегодня почти нечего сказать: собранный молодыми музыкантами, некоторые из которых едва-едва отметили двадцатилетие, он исполняет авторскую музыку. Даже в лучшие годы джаза надо было быть каким-то совершенным гением, чтобы к такому возрасту быть узнаваемым на международном уровне с собственным материалом. А эти молодые парни ещё и отказываются переезжать в столицу страны, продолжая постоянно жить, учиться и работать в Орхусе, втором по значению городе Дании с населением около трёхсот тысяч человек. Однако есть один существенный аргумент в пользу того, что BBNJQ — не просто очередной талантливый студенческий ансамбль, которому предстоит по окончании его участниками образовательного процесса немедленно разлететься на осколки и напитать собой коллективы маститых лидеров старшего поколения. И называется этот аргумент исчерпывающе: Danish Music Award 2014. Получить национальную музыкальную премию в номинации «лучший новый джазовый артист года» — это, с какой стороны ни посмотри, достижение. Обязаны ему музыканты своим дебютным диском, даже не получившим какого-то собственного имени: просто «Bangin’ Bülows Nice Jazz Quartet».

В том же 2014 году группа предприняла небольшой тур в несколько неожиданном направлении, отправившись в Гвинейскую республику (Западная Африка). На этом фоне предстоящий месяц в России в рамках «МузЭнергоТура» — что и говорить, не такая уж экзотика. Но в любом случае можно говорить о том, что и у самого коллектива налицо стремительный карьерный рост, и у «МузЭнергоТура» в обойме самые что ни на есть актуальные и признанные «молодые львы» европейского джаза, определяющие его завтрашнее лицо. Science Fiction Theater (Швейцария) Science Fiction Theater (Швейцария)
http://vk.com/video-92493518_170979475?list=1dd680cdac64dbe61b Science Fiction Theater (Швейцария)
http://vk.com/video-92493518_170980033?list=19197ef43ff922b585 Участники Science Fiction Theater (Швейцария) Science Fiction Theater (Швейцария)
Говоря о специфике Science Fiction Theater, лидер пользуется умопомрачительным и трудно поддающимся переводу набором терминов: «Surf – Movie Sounds – Nu Trash – Easy Listening». Очевидны здесь разве что «звуки кино»: музыка этого коллектива представляет собой своего рода альтернативную озвучку к старым фильмам (а обычно — к безумной нарезке старых фильмов). Всё остальное столь же удачно сформулировано, сколь и серьёзно. Пожалуй, главное достоинство создаваемой Кристофом и его товарищами «новой музыки» именно в том. что она далека от привычного уже сегодня стёба; Граб совсем не выглядит этаким российским Гоблином, создающим вымышленные переводы к существующему кино и делающим ставку на то, что аудитория будет радостно смеяться. Вместо этого он вполне честно и не с большей иронией, чем делал бы это в обычной жизни, находит совершенно новые смыслы в том материале, которым вдохновляется. Порой это получается в куда более шизофренической манере, чем в самых своих диких мечтах мог бы предполагать режиссёр; порой градус сумасшествия в музыке превращает происходящее в тотальный абсурд; но порой прорезается и лирика, которую крайне трудно бы было заподозрить как в оригинальном видеоматериале, так и в современном европейском музыканте, претендующем на «nu trash». Неудивительно, что работы Science Fiction Theater получают восторженные отзывы у критиков и слушателей.

Любопытно, что в составе коллектива играет давно живущий в Цюрихе басист Илья Комаров, выходец из Таллина, весьма хорошо известный российской публике по эстонскому коллективу «Не ждали», громко заявившему о себе на рубеже 1980-х и 1990-х. Science Fiction Theater (Швейцария)

Christoph Grab (саксофоны, электроника)
Felix Utzinger (гитара)
Marcel Thomi (клавишные)
Ilja Komarov (бас)
Andy Wettstein (ударные)

Один из многочисленных коллективов, в которых определяющую роль играет саксофонист и бас-кларнетист из Цюриха Кристоф Граб — хрестоматийный представитель сегодняшней швейцарской импровизационной сцены, отчётливо оригинальной и одновременно разноплановой настолько, насколько это возможно. Среди тех, у кого Граб учился — великие Джо Ловано и Дэйв Либман; среди тех, с кем он выступал — артисты в диапазоне от Бенни Голсона до Луи Склави; среди стран, в которые он отправлялся со своей музыкой — Франция и Литва, Испания и Египет, Россия и Сербия. Список одних только швейцарских коллективов, в которых работал Граб, включает десятки наименований, и именно этот список объясняет его путь к Science Fiction Theater, одному из главных на сегодня проектов Кристофа. Среди его ранних увлечений — например, Kurt Weill Vibes Revisited, ContempArabic Jazz Ensemble, Neuromodulator: все эти названия говорят сами за себя. Граб крайне активно работает с электроникой, используя, помимо вполне традиционных (вернее, предсказуемых) устройств обработки и то, что он сам называет «взломанными игрушками». Как результат, к нему с большим уважением относятся и представители электронной музыки, и многие европейские диджеи — и это при том, что исполнение Граб совмещает с профессорским креслом Университета искусств Цюриха. Итак,представляем участников тура - 2015:

Science Fiction Theater (Швейцария) 
Daniel Herskedal (Норвегия)
Rocqawali (Пакистан/Дания)
Svin (Дания)
Spinifex (Нидерланды)
Bangin' Bulows Nice Jazz Quartet (Дания)
Сводный "МузЭнергоБэнд" (Испания/Франция/Швейцария)
 
Расскажем о каждом коллективе поподробнее.  Ну что, друзья? 16 июня, во вторник, "МузЭнергоТур" №3 снова в Электростали. ДК им. Карла Маркса ждёт нас, а вас ждёт совершенно безумный музыкальный микс - от норвежской тубы соло, приправленной электроникой, до пакистанской суфийской музыки каввали, аранжированной в стилистике хард-рока 70-х. И, конечно, джаз.

А пока мы хотели бы у вас спросить: вот такая афиша (именно как средство привлечения людей на концерт именно в Электростали, будучи наклеенной на стену, а не картинка в интернете) - как вам?  МузЭнергоТур-2015 вовсю в процессе планирования. Электросталь весьма не исключена где-то в районе 15-19 июня. А вот что началось уже сейчас - так это краудфандинговая кампания по ФИЛЬМУ о "МузЭнергоТуре", которая позволить всем узнать, как же всё это выглядит до и после того, как автобус с сумасшедшими музыкантами приезжает в город. Тур мы сделаем и сами, на то мы и организаторы. На фильм же не хватает ни средств, ни сил. Зато если общество поможет - леденящие кровь закулисные подробности увидят все-все-все.

Присоединяйтесь, друзья! За кулисами иногда не менее интересно, чем на концертах.

http://planeta.ru/campaigns/13047  Эгегей! сегодня! 18:30, в ДК им. Васильева (Карла Маркса, 7)!

Мы уже приехали и ждём всех на концерт, которые по ряду причин не очень удалось проаносировать так, как стоило бы - и потому распространяйте новости, друзья!

Билеты 300-600 рублей на входе.

Участвуют:

- MARC EGEA (Испания)
- MAD KLUSTER TRIO (Франция)
- ANGEL ONTALVA & VASCO TRILLA (Испания/Португалия)
- HELLMULLER RISSO ZANOLI (Швейцария/Италия)
- AUTHENTIC LIGHT ORCHESTRA (Швейцария/Армения)        20 июня. "МузЭнергоТур"  Ну что, друзья. Электросталь. 20 июня. "МузЭнергоТур" возвращается. Так уж получилось, что на сей раз не ДК Карла Маркса, где не было свободной даты, а в КЦ им. Васильева. Но от этого вся история не делается менее интересной ;)   Магазин рекордеров ZOOM

https://vk.com/club65717720

ВСТУПАЙТЕ! Поздравляем всех Татьян, 
Дочек, бабушек и мам!
Пусть вам счастье улыбнётся, 
В жизни путь осветит солнце.
Будьте радостны, желанны… 
Поздравляем Вас, Татьяны!     Выражаем благодарность за проведение видеосъемки агентству "Ваши праздники.РУ" (  http://vk.com/vashiprazdnikiru ) и непосредственно оператору Анатолию. Концерт состоялся, благодарим всех кто пришел и поддержал нас в продвижении данного музыкального проекта, надеемся всем понравилось, мы старались) СЕГОДНЯ!!! Спешите быть!!! Билеты в кассе ДК!!!  Завтра!!! Спешите быть!!! Билеты в кассе ДК!!!  Осталось 3 дня!!! Спешите быть!!! Билеты в кассе ДК!!! Сергей Онищенко с проектом [club39719992|Make Like a Tree] на МузЭнерго 24 июня в Электростали! 
Бард-фолк от путешествующего музыканта - ещё одно доказательно того, как многомерно стилистическое измерение [club2918822|МузЭнерго].. )) Осталось 5 дней!!! Спешите быть!!! Билеты в кассе ДК!!! ...подробности об удивительных участниках тура и фестиваля:
Raimundo Santander & La Orquesta Del Viento (Чили)

"Особую прелесть работе Сантандера и его товарищей придаёт то, что подобный симбиоз — именно симбиоз, а не коммерчески-ориентированное усиление чего-то более важного чем-то более броским. «Оркестр Ветра» с одинаковым желанием и развивает джаз через близкие им фольклорные идеи, и нащупывает новые пути для чилийской народной музыки через импровизационный подход." ([id11028606|Юрий Льноградский])
http://tour.muzenergo.ru/viento/  Встречайте 24 июня в Электростали, среди 6 удивительных коллективов дуэт: Alain Blesing (гитара) и Fred Roudet (труба).

Творческий путь Фреда Руде типичен для французского джазмена национального уровня известности: десятки самых разноплановых коллективов, работа в над музыкой для кино и театральных постановок, специальные проекты в области соединения европейского джаза с музыкальными традициями других стран и континентов, участие в мультистилистических ассоциациях. 

Что до гитариста Алена Блесина, то его дорога в музыке несколько более самостоятельна: с середины 1970-х он входит в число основных фигур французского прогрессив-рока (знатоки до сих пор ценят такие группы с его участием, как Eskaton и Arsenal). Джаз Ален открыл для себя уже позже, начав им заниматься в первой половине 1980-х. А к концу 1980-х список наиболее интересующих его направлений пополнился традиционной фольклорной музыке — во многом благодаря знакомству с турецкой вокалисткой Сэнэм Дийиджи, впоследствии ставшей женой музыканта.

([id11028606|Юрий Льноградский]) А ещё на фестивале 24 июня вы сможете стать свидетелями того, какова в действии идея проекта Make Like a Tree... 

Make Like a Tree — это музыкальный проект харьковчанина Сергея Онищенко, основная идея которого в том, что вся музыка создаётся и записывается в процессе путешествий в разных уголках планеты. В записях и выступлениях принимают участие музыканты из самых разных городов и стран. У проекта нет постоянного состава, каждый концерт проходит с новыми участниками и может превратить Make Like A Tree как в дуэт, так и в достаточно крупный импровизирующий ансамбль. Основой для музицирования служат тексты, создаваемые Сергеем на русском и английских языках, и некая базовая гармоническая сетка; в остальном же импровизация вовлечённых в ансамбль участников предполагает самые неожиданные повороты, могущие превратить одну и ту же композицию и в бард-рок классического толка в стилистике раннего Боба Дилана, и в медитационное новоджазовое действо... 

Такую музыку лучше воспринимать в компании друзей или родных и близких, ведь это, по сути, сотворчество, в очень доброй атмосфере.

http://tour.muzenergo.ru/make-like-a-tree/ Друзья, напоминаем, один из участников фестиваля: Lucien Dubuis Trio - трио - музыка, которую дают баритон-гитара, альт-саксофон, контрабас-кларнет в сочетании с юмором, а также идеями свободы и игры! 

Выступление 24 июня в Электростали для музыкантов будет всего лишь 3-им днём [club53363855|МузЭнергоТура] из 24 дней путешествия!

Вы уже позвали всех друзей? точно-точно? Свежий альбом участника МузЭнергоТура - Happy55 (Воронеж) Встречайте 24 июня в Электростали, среди 6 удивительных коллективов дуэт Фреда Руде и Алена Блесина!

Это да французских музыканта сотрудничают друг с другом в весьма разных формациях: Mavi Yol 4tet, OctoBando, Mad Kluster. Это разная, но всегда интересная музыка, находящаяся на переднем крае современного европейского искусства, ещё не оторванного от думающих масс. 

Если в их музыке проглядывает традиционный свинг — он всегда осовременен до состояния, не оставляющего места чистой развлекательности; если эта музыка оказывается свободной, в ней всегда присутствует драматургия и диалог и никогда нет места чистому хаосу. 

Критики называют дуэт Блесина и Руде одним из последних посвящений мелодии в мире тотального авангарда — и они однозначно правы: свободная музыка действительно может быть мелодичной, плавной, умудрённой и светлой. Немного отзывов из анкет посетителей А ещё на нашем фестивале 24 июня выступит «Оркестр Ветра» из Чили !!!
(Raimundo Santander & La Orquesta Del Viento). 

«La Orquesta del Viento» — в переводе «Оркестр Ветра». 
Этот образ хорошо описывает творческий метод молодого гитариста Раймундо Сантандера, называемого сегодня одним из интереснейших чилийских джазменов.
Ветер — это вообще удачный образ для Чили, страны во всех смыслах необычной. Так же, как в её культуре смешаны самые разные влияния, в материале «Оркестра» смешаны джаз как образующая форма и национальный фольклор Анд и Южной Америки в целом как мелодический вектор. 
Особую же прелесть работе Сантандера и его товарищей придаёт то, что подобный симбиоз — именно симбиоз, а не коммерчески-ориентированное усиление чего-то более важного чем-то более броским. «Оркестр Ветра» с одинаковым желанием и развивает джаз через близкие им фольклорные идеи, и нащупывает новые пути для чилийской народной музыки через импровизационный подход.

http://tour.muzenergo.ru/viento/ Баритон-гитара, альт-саксофон, контрабас-кларнет в сочетании с юмором, а также идеями свободы и игры, свойственными швейцарской команде Lucien Dubuis Trio, рождают музыку, которая, по словам продюсера МузЭнерго [id11028606|Юрия Льноградского]  непроста, колюча, привлекательна и по-хорошему безумна". 

А вообще... как сказал великий Марк Рибо, «Если вам действительно интересно, что такое downtown образца 1982 года, сходите и послушайте этих швейцарских парней, которым едва за двадцать».

Не забывайте звать друзей и близких! Каждый фестиваль в рамках проекта [club2918822|МузЭнерго]  - это действительно, неповторимое событие, потому что каждый исполнитель - в буквальном смысле слова добывается как алмаз из общей творческой шахты. Друзья! Один из участников тура и фестиваля в Электростали - [club833177|HAPPY55]  
Их музыка ведёт своё происхождение от принципов свободной ансамблевой импровизации и одновременно… эйсид-джаза, как ни парадоксально это звучит.
Результат — современная, умная, необычная ритмически и гармонически музыка, в которой найдёт своё место и ценитель джазовой традиции, и завсегдатай ночного клуба. 

Зовите друзей слушать один из самых интересных коллективов современной России, исполняющих импровизационный авторский материал! Все вопросы в кассу по тел 57-7-35-04 БИЛЕТЫ В КАССЕ! 
 
Часы работы кассы: 
 
Пн-Чт: 11-00 - 19-30 
 
Пт: 11-00 - 18-30 
 
Сб - Вс: 12-00 - 16-00 

Обед: 13-00 - 14-00

(496) 577-35-04, (496) 577-36-55

Адрес: улица Карла Маркса, 9 Билеты поступили в продажу в кассе ДК К- Маркса!!! 